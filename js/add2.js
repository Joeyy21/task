$(document).ready(function (){
    $('#type').change(function (){
        let type = $(this).val()
        $('#'+type).removeClass('visually-hidden');

        $.each(['Dvd','Book','Furniture'],function (index,type) {
            if($('#type').val() !== type)
            {
                $('#'+type).addClass('visually-hidden')
            }
        })


    })

    $('#Add').click(function (){
        let type = $('#type')
        if(type.val() != null)
        {
            type.removeClass('is-invalid')
            let SKU = $('#SKU').val()
            let name = $('#name').val()
            let price = $('#price').val()
            let specs = {
                size_MB:$('#size_MB').val(),
                size_KG:$('#size_KG').val(),
                Height:$('#Height').val(),
                Width:$('#Width').val(),
                Length:$('#Length').val()
            }

            $.post('insert.php',{
                    type:type.val(),
                    SKU:SKU,
                    P_name:name,
                    price:price,
                    specs: specs
                },
                function (data,status) {
                    let response = JSON.parse(data)


                    if(response['valid'])
                    {
                        $('.invalid-feedback').css({
                            display: "none"
                        });

                        let alert_box =$('#success_message')
                        alert_box.hide();
                        $('input').val('');
                        alert_box.html('<div class="col-md-8">\n' +
                            '<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
                            '     <strong>Success!!</strong> You have added the product successfully. \n' +
                            '     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>\n' +
                            '</div>\n' +
                            '</div>');
                        alert_box.show(350);

                        setTimeout( function() { location.replace('product_list.php')}, 2000 );
                    }
                    else
                    {
                        $('.invalid-feedback').css({
                            display: "block"
                        });



                        if (response['messages']['SKU']) {
                            $('#SKU_E').html(response['messages']['SKU']);
                        }else {
                            $('#SKU_E').html('');
                        }

                        if (response['messages']['P_name']) {
                            $('#name_E').html(response['messages']['P_name']);

                        }else {
                            $('#name_E').html('');
                        }

                        if (response['messages']['price']) {
                            $('#price_E').html(response['messages']['price']);
                        }else {
                            $('#price_E').html('');
                        }


                        if (response['messages']['price']) {
                            $('#price_E').html(response['messages']['price']);
                        }else {
                            $('#price_E').html('');
                        }


                        if (response['messages']['size_MB']) {
                            $('#size_MB_E').html(response['messages']['size_MB']);
                        }else {
                            $('#size_MB_E').html('');
                        }


                        if (response['messages']['size_KG']) {
                            $('#size_KG_E').html(response['messages']['size_KG']);
                        }else {
                            $('#size_KG_E').html('');
                        }


                        if (response['messages']['Height']) {
                            $('#Height_E').html(response['messages']['Height']);
                        }else {
                            $('#Height_E').html('');
                        }


                        if (response['messages']['Width']) {
                            $('#Width_E').html(response['messages']['Width']);
                        }else {
                            $('#Width_E').html('');
                        }


                        if (response['messages']['Length']) {
                            $('#Length_E').html(response['messages']['Length']);
                        }else {
                            $('#Length_E').html('');
                        }
                    }

                })

        }
        else
        {
            type.addClass('is-invalid')
        }


    });
})