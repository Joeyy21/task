<?php
//Auto loading the classes
include_once ('autoload.php');


//the name of the class we get
$className = 'Specifications\\'.$_POST['type'];

//initializing the class for the product.
$product  = new $className($_POST['SKU'],$_POST['P_name'],$_POST['price'],$_POST['type'],$_POST['specs']);


$dbh = new DB\DBconfig(); // Connecting to the database.


// inserting into database and getting if the error messages and if there was an insertion or not
$result= json_encode($product->insert($dbh->con));

echo $result;

$dbh->con = null; // closing the connection for the DB