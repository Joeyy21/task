<?php


namespace Specifications;


class Furniture extends Product
{
    protected $Height;
    protected $Width;
    protected $Length;

    public function __construct($SKU,$name,$price,$type,$data=null)
    {

        $this->assigning_standard_inputs($SKU,$name,$price,$type);

        if(isset($data))
        {
            $this->Height = $data['Height'];
            $this->Width = $data['Width'];
            $this->Length = $data['Length'];
        }


    }

    private  function validateSpec() // for validating the furniture dimensions
    {
        if($this->Height == "")
        {
            $this->messages['Height'] = "Please enter a height for the furniture !!";
            $this->valid = false;
        }
        else
        {
            if(!filter_var($this->Height,FILTER_VALIDATE_FLOAT))
            {
                $this->messages['Height'] = "Please enter a correct Height !!";
                $this->valid = false;
            }
        }

        if($this->Width == "")
        {
            $this->messages['Width'] = "Please enter a Width for the furniture !!";
            $this->valid = false;
        }
        else
        {
            if(!filter_var($this->Width,FILTER_VALIDATE_FLOAT))
            {
                $this->messages['Width'] = "Please enter a correct Width !!";
                $this->valid = false;
            }
        }

        if($this->Length == "")
        {
            $this->messages['Length'] = "Please enter a Length for the furniture !!";
            $this->valid = false;
        }
        else
        {
            if(!filter_var($this->Length,FILTER_VALIDATE_FLOAT))
            {
                $this->messages['Length'] = "Please enter a correct Length !!";
                $this->valid = false;
            }
        }
    }

    public function insert($con)
    {
        $this->validating_standard_inputs($con);
        $this->validateSpec();
        if($this->valid)
        {
            try
            {
                // Beginning a transaction
                $con->beginTransaction();

                $sql = "INSERT INTO product(SKU,P_name,price,P_type) VALUES ('$this->SKU','$this->name',$this->price,'$this->type')";
                $query = $con->prepare($sql);
                $query->execute();
                $Id = $con->lastInsertId();

                $sql2 = "INSERT INTO Furniture (ID, Height, Width, Length) VALUES ($Id, $this->Height, $this->Width, $this->Length)";
                $query2 = $con->prepare($sql2);
                $query2->execute();

                if(!($query2->rowCount()>0) || !($query->rowCount()>0))
                {
                    $con->rollback();
                    $this->messages['insertions'] = "Error in insertion";
                    $this->valid = false;
                    return ["valid"=>$this->valid,"messages"=>$this->messages];
                }

                //applying changes to the database
                $con->commit();
                return ["valid"=>$this->valid,"messages"=>$this->messages]; //getting the response

            }
            catch (\Exception $e)
            {
                //rolling back if there was any error in any one of the two sql statements.
                $con->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        }
        else
        {
            return ["valid"=>$this->valid,"messages"=>$this->messages];
        }
    }

    public function getSpecs($id, \PDO $con)
    {
        $sql = "SELECT * FROM $this->type WHERE ID =".$id;
        $query = $con->prepare($sql);
        $query->execute();

        $specs = $query->fetch(\PDO::FETCH_ASSOC);

        if($query->rowCount() > 0)
        {
            return "Dimensions: ".$specs['Height']."x".$specs['Width']."x".$specs['Length']."<br/>";
        }

    }

}