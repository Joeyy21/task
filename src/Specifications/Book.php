<?php


namespace Specifications;

class Book extends Product
{
    protected $size_KG;

    public function __construct($SKU,$name,$price,$type,$data=null)
    {

        $this->assigning_standard_inputs($SKU,$name,$price,$type);

        if (isset($data))
        {
            $this->size_KG = $data['size_KG'];
        }



    }

    private  function validateSpec() // for validating the book size
    {

        if($this->size_KG == "")
        {
            $this->messages['size_KG'] = "Please enter a size !!";
            $this->valid = false;
        }
        else
        {
            if(!filter_var($this->size_KG,FILTER_VALIDATE_FLOAT))
            {
                $this->messages['size_KG'] = "Please enter a correct size !!";
                $this->valid = false;
            }
        }

    }

    public function insert(\PDO $con)
    {
        $this->validating_standard_inputs($con);
        $this->validateSpec();
        if($this->valid)
        {
            try
            {
                // Beginning a transaction
                $con->beginTransaction();

                $sql = "INSERT INTO product(SKU,P_name,price,P_type) VALUES ('$this->SKU','$this->name',$this->price,'$this->type')";
                $query = $con->prepare($sql);
                $query->execute();
                $Id = $con->lastInsertId();

                $sql2 = "INSERT INTO Book (ID,size_KG) VALUES ($Id,$this->size_KG)";
                $query2 = $con->prepare($sql2);
                $query2->execute();

                if(!($query2->rowCount()>0) || !($query->rowCount()>0))
                {
                    $con->rollback();
                    $this->messages['insertions'] = "Error in insertion";
                    $this->valid = false;
                    return ["valid"=>$this->valid,"messages"=>$this->messages];
                }

                //applying changes to the database
                $con->commit();
                return ["valid"=>$this->valid,"messages"=>$this->messages]; //The response we need

            }
            catch (\Exception $e)
            {
                //rolling back if there was any error in any one of the two sql statements.
                $con->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        }
        else
        {
            return ["valid"=>$this->valid,"messages"=>$this->messages];
        }
    }


    public function getSpecs($id, \PDO $con)
    {
        $sql = "SELECT * FROM $this->type WHERE ID =".$id;
        $query = $con->prepare($sql);
        $query->execute();

        $specs = $query->fetch(\PDO::FETCH_ASSOC);

        if($query->rowCount() > 0)
        {

            return "Size: ".$specs['size_KG']." Kg<br/>";

        }
        else
        {
            return $sql;
        }
    }
}