<?php


namespace Specifications;


class Product
{
    protected $SKU;
    protected $name;
    protected $price;
    protected $type;
    protected $messages = array(); // Array for the error messages
    protected $valid = true; // Flag as an indicator if all the attributes are valid for insertion or not.

    protected function assigning_standard_inputs($SKU,$name,$price,$type)
    {
        $this->SKU = filter_var(trim($SKU),FILTER_SANITIZE_STRING);
        $this->name = filter_var(trim($name),FILTER_SANITIZE_STRING);
        $this->price = $price;
        $this->type = $type;
    }

    protected function validating_standard_inputs($con) // validating the standard inputs
    {

        if($this->SKU == "")
        {
            $this->messages['SKU'] = "Please enter an SKU !!";
            $this->valid = false;

        }
        else
        {
            //validating for the uniqueness of SKU
            $sql = "SELECT * FROM product WHERE SKU = :sku";
            $query = $con->prepare($sql);
            $query->bindParam(':sku',$this->SKU,\PDO::PARAM_STR);
            $query->execute();
            if($query->rowCount()>0)
            {
                $this->messages['SKU'] = "This SKU already exists, please enter another one !!";
                $this->valid = false;
            }
        }


        if($this->name == "")
        {
            $this->messages['P_name'] = "Please enter a name for the product !!";
            $this->valid = false;
        }


        if($this->price == "")
        {
            $this->messages['price'] = "Please enter a price !!";
            $this->valid = false;
        }
        else
        {
            if(!filter_var($this->price,FILTER_VALIDATE_FLOAT))
            {
                $this->messages['price'] = "Please enter a correct price !!";
                $this->valid = false;
            }
        }

    }

    static function getProducts(\PDO $con)
    {
        $sql = "SELECT * FROM product";
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($results as $result)
        {

            $className = 'Specifications\\' . $result['P_type'];
            $product = new $className($result['SKU'], $result['P_name'], $result['price'], $result['P_type']);

            $specs = $product->getSpecs($result['ID'], $con);

            echo  '<div class="col-md-4 mb-2 mt-2">
                            <div class="card text-dark">
                                <div class="card-header">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="Delete['.$result['ID'].']"
                                               value="'.$result['ID'].'" id="defaultCheck'.$result['ID'].'"/>
                                        <label class="form-check-label" for="defaultCheck'.$result['ID'].'">
                                            Select
                                        </label>
                                    </div>
                                </div>
                                <div class="card-body text-center">
                                    <h5 class="card-title">'.$result['P_name'].'</h5>
                                <p class="card-text">
                                    SKU:'.$result['SKU'].'<br/>
                                    Price:'.$result['price'].'<br/>
                                    '.$specs.'
                                </p>
                            </div>
                        </div>
                </div>';
        }

    }

}