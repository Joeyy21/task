<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/add2.js"></script>
    <script src="js/show.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>

<div class="container">
    <form method="post" action="massDelete.php">


        <div class="card mt-3">
            <div class="card-header bg-light">
                <div class="row">
                    <div class="col-md-4 h4">
                        Product List
                    </div>

                    <div class="col-md-4 offset-4 text-end">
                        <a class="btn" href="add.php"><button type="button" class="btn btn-primary" name="Add" id="Add">Add</button></a>
                        <button type="submit" class="btn btn-danger" name="massDelete" id="massDelete">Mass Delete</button>
                    </div>

                </div>
            </div>
            <div class="card-body bg-dark text-white">
                <div id="products" class="row justify-content-center">



                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>