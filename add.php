<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/add2.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
    <div class="row justify-content-center mt-5 mb-5">
        <div class="col-md-8">
            <div id="success_message" class="row justify-content-center">



            </div>

            <form method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header bg-light">
                        <div class="row">
                            <div class="col-md-4 h4">
                                ADD PRODUCT
                            </div>

                            <div class="col-md-4 offset-4 text-end">
                                <button type="button" class="btn btn-primary" name="Add" id="Add">Add</button>
                                <a class="btn" href="product_list.php"><button type="button" class="btn btn-danger" name="Cancel" id="Cancel">Cancel</button></a>
                            </div>

                        </div>
                    </div>
                    <div class="card-body bg-dark text-white">

                        <div class="mb-3">
                            <label for="SKU" class="form-label">SKU</label>
                            <input type="text" name="SKU" class="form-control" id="SKU" placeholder="SKU">
                            <small id="SKU_E" class="invalid-feedback"></small>
                        </div>

                        <div class="mb-3">
                            <label for="name" class="form-label">Product Name</label>
                            <input type="text" class="form-control" name="P_name" id="name" placeholder="Product Name">
                            <small id="name_E" class="invalid-feedback"></small>
                        </div>

                        <div class="mb-3">
                            <label for="price" class="form-label">Product Price</label>
                            <input type="number" class="form-control" name="price" id="price" placeholder="Product Price $">
                            <small id="price_E" class="invalid-feedback"></small>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="type">Product Type</label>
                            <select name="type" class="form-select" id="type">
                                <option selected hidden disabled>Choose...</option>
                                <option value="Dvd">Dvd</option>
                                <option value="Book">Book</option>
                                <option value="Furniture">Furniture</option>
                            </select>
                        </div>

                        <div id="Dvd" class="mb-3 visually-hidden">
                            <div class="row justify-content-center">
                                please enter the size of the Dvd.
                            </div>

                            <div class="row mx-1">
                                <label for="size_MB" class="form-label">DVD Size in MB</label>
                                <input type="number" class="form-control" name="size_MB" id="size_MB" placeholder="Size in MB">
                                <small id="size_MB_E" class="invalid-feedback"></small>
                            </div>

                        </div>

                        <div id="Book" class="mb-3 visually-hidden">
                            <div class="row justify-content-center">
                                please enter the size of the Book.
                            </div>

                            <div class="row mx-1">
                                <label for="size_KG" class="form-label">Book Size in Kg</label>
                                <input type="number" class="form-control" name="size_KG" id="size_KG" placeholder="Size in Kg">
                                <small id="size_KG_E" class="invalid-feedback"></small>
                            </div>

                        </div>

                        <div id="Furniture" class="mb-3 visually-hidden">
                            <div class="row mb-2 justify-content-center">
                                please enter the dimension of the Furniture.
                            </div>

                            <div class="row mx-1">

                                <div class="col-sm-4">
                                    <label class="visually-hidden" for="Height">Height</label>
                                    <input type="number" class="form-control" id="Height" placeholder="Height">
                                    <small id="Height_E" class="invalid-feedback"></small>
                                </div>

                                <div class="col-sm-4">
                                    <label class="visually-hidden" for="Width">Width</label>
                                    <input type="number" class="form-control" id="Width" placeholder="Width">
                                    <small id="Width_E" class="invalid-feedback"></small>
                                </div>

                                <div class="col-sm-4">
                                    <label class="visually-hidden" for="Length">Length</label>
                                    <input type="number" class="form-control" id="Length" placeholder="Length">
                                    <small id="Length_E" class="invalid-feedback"></small>
                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </form>

        </div>

    </div>
</div>

</body>
</html>